<h2>Introduction</h2>

<p>Your task in this quiz is to extend your quiz 6 codebase with the feature of new translations into different languages, and to document the interface to your translation feature so that a translator is able to understand how to use it. This is an open-ended exercise: there are many different potential solutions.</p>

<p>You can find two translations for each of the strings in the quiz 6 program at <a href="https://docs.google.com/spreadsheets/d/1w0J4vwGRBFHC2_0oY-JPvwsYinARa2ScNR4Rl_0HbEg/edit?usp=sharing">this link</a>.</p>

<p>Your code must implement at least the two provided translations from this spreadsheet, and you should try to design your code so that it's easy to add more translations. You're welcome to write your own additional translations to add to your code if you want, but that's not the goal of this exercise.</p>

<p>Please review the lecture videos if you need to! This quiz touches on many of the topics we've been discussing over the past couple weeks.</p>


<h2>Setup</h2>

<p>You should start with a copy of your code from quiz 6. You may refactor your quiz 6 code in any way that you want. By the end of your work in this quiz, your code should behave the same as your code in quiz 6, but with the additional feature of translations.</p>

<p>In your new copied project folder, run the command <code>npm i --save-dev typedoc</code>. This will install the <code>typedoc</code> tool, which can generate browsable HTML documentation from documentation comments as we've discussed in lecture.</p>

<p>To generate the browsable HTML documentation for your project, run the command <code>npx typedoc</code> from your project folder. To browse the generated documentation, open the <code>docs/index.html</code> file in your web browser. When your documentation comments change, you will need to re-run <code>npx typedoc</code> and reload the page in your browser in order to see the updates.</p>


<h2>Code requirements</h2>

<p>At the very start of the game, before anything else is printed, your code should print a message like this using <code>prompt</code>:</p>
<blockquote>
  Enter EN to play in English.<br>
  Masukkan ID untuk bermain dalam Bahasa Indonesia.<br>
  「JA」を入れて日本語で遊ぶ。
</blockquote>

<p>This message should include all of the languages that your code supports: if your code is extended with new translations later, this message should get updated. The two-letter language identifiers like EN, ID, and JA are <a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639-1</a> codes. The user will enter one of these identifiers (in uppercase) to select their language. If the user enters an invalid identifier, the program should print the string "Invalid input." in <i>every</i> language that your code supports, using a single call to <code>console.err</code> (not a separate call for each language).</p>

<p>After this point, the program should do all of its input and output in the language that the user has selected. If the program is restarted by calling <code>play()</code> again, it should ask for the user's language again.</p>

<p>You may use any TypeScript features and any programming techniques in your code, but pay attention to the hints below for help on how to apply the material we've covered in lecture.</p>

<p>You may modify the <code>src/Main.ts</code> file and add additional <code>.ts</code> files inside the <code>src</code> folder, but you <b>must not modify any of the project settings</b> outside the <code>src</code> folder (<code>package.json</code>, <code>tsconfig.json</code>, etc.).</p>


<h2>Documentation requirements</h2>

<p>Imagine you are working with a team of translators, who will be adding new translations to this codebase over time. In addition to implementing the two translations that already exist, your job is to make it easy for new translations to be added later.</p>

<p>Your translation code should provide a <b>public interface</b> that allows clients to <b>add new translations to the game <i>without modifying any existing code</i></b>. You do not necessarily have to provide an actual <code>interface</code> type, although you may want to; refer to lecture for our discussion on what a "public interface" is.</p>

<p>The imaginary team of translators speaks English as a common language, so you should <b>provide English documentation that instructs a translator how to add a new translation to your code</b>.</p>

<p>You can assume the translators know TypeScript as well, so you should <b>not</b> write documentation on how to use TypeScript itself. You should document anything that might be non-obvious to a TypeScript programmer about how to use the interface that you provide.</p>

<p>Your documentation <b>must be written in the form of documentation comments</b> that <code>typedoc</code> recognizes.</p>


<h2>Hints</h2>

Here are some hints to help get you started on adding the features described above.

<h3>Imports</h3>

<p>If you want to structure your code across multiple files, you must use the <code>export</code> keyword to declare any types, variables, or functions that you want to access from a different file. Anything declared without <code>export</code> is "private" to the file it's declared in.</p>

<p>To import something named <code>foo</code> from a file named <code>Bar.ts</code> in the same folder as the current file, write <code>import { foo } from "./Bar"</code>. See the assignment 1 and 2 code for some examples.</p>


<h3>Enum types</h3>

<p>TypeScript has an <code>enum</code> keyword that you may want to use in your translation code, but it's a little annoying to use. You'll probably get the best results if you define your enum like this:</p>
<pre>
enum Example {
  Foo = "Foo",
  Bar = "Bar",
  // etc.
}
</pre>

<p>With this definition of <code>Example</code>, if you have a <code>string</code> value <code>x</code>, you can write <code>if (x in Example) ...</code> to check whether <code>x</code> is one of the values defined in the enum.</p>

<h3>Resource usage</h3>

<p>There's no need to hand-optimize your code in this setting: any modern browser will run this small program quickly enough for a user to be satisfied with.</p>

<h3>OOP or not?</h3>

<p>For this exercise, the game itself is considered "finished": there will not be any new gameplay features added to the game over time. In other words, the set of strings in the game is not expected to change later.</p>

<p>The set of translations <b>is</b> expected to change over time, as translators add new translations to the existing gameplay.</p>

<p>Consider carefully whether this seems like "adding new behaviors to existing data" or "adding new data to existing behaviors". This should inform your choice of how to structure the code, as we've discussed in lecture.</p>


<h2>Submitting your code</h2>

<p>When you're finished, zip up your <code>src</code> folder and upload it to this page. Don't include your <code>docs</code> folder - we will generate that from your <code>src</code> folder using <code>typedoc</code>.</p>
