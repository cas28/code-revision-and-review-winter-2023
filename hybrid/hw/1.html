<html>
  <body>

    <h2>Introduction</h2>

      <p>For our first exercise, you're going to fork a Git repository containing a little calculator application and add some features to it.</p>

      <p>This isn't <i>directly</i> related to code review, but it's likely that many students in this course, especially undergraduates, have never been part of a modern collaborative software development process. I think that before we can really begin talking about reading and reviewing code, it's important to work through an illustration of that kind of process, so that everyone has a baseline for what it looks like and what the tools we use are capable of.</p>

      <p>In some ways, this assignment is also a test of your ability to read instructions carefully and follow them correctly. You should re-read this document many times before you start writing any code.</p>

      <p>If you're struggling, it will help to review the lecture videos!</p>

      <p>Now: imagine you just started a job, and your first task before you start any real work is to learn how to build, read, and modify the codebase with the tools that your team uses.</p>


    <h2>Setup</h2>

    <p>First, make sure you've followed the setup instructions in the <a href="https://gitlab.cecs.pdx.edu/cas28/typescript-intro">TypeScript intro project</a>.</p>

    <p>You should be able to access the codebase for this assignment at <a href="https://gitlab.cecs.pdx.edu/cas28/calculator">https://gitlab.cecs.pdx.edu/cas28/calculator</a>, using the LDAP login with the same credentials that you use to sign onto the CS department Linux servers (your MCECS credentials). Do not download this project directly: instead, click the "fork" button near the upper-right corner of this page to create your own fork (a copy of the codebase that you are the owner of).</p>

    <p>You should be taken to your new fork. Take note of the URL. <b>THIS NEXT STEP IS <i>CRITICALLY</i> IMPORTANT: WE WILL BE <i>UNABLE</i> TO GRADE YOUR CODE IF YOU DO NOT FOLLOW THESE INSTRUCTIONS CORRECTLY.</b> Click "Settings" in the GitLab menu and expand the "Visibility, project features, permissions" section, and then set the "Project visibility" dropdown to "Internal" and set the "Issues", "Repository", "Merge requests", and "Forks" dropdowns to "Only project members". Then, click "Members" in the GitLab "Project Information" submenu and add me (<code>@cas28</code>) with a "Guest" role permission. <b>Make sure to click the "Save" button after changing the settings.</b></p>

    <p>Finally, follow <a href="https://docs.gitlab.com/ee/user/ssh.html">these instructions from GitLab</a> to set up an SSH key on your GitLab account, which will be the "password" that VSCode automatically uses when communicating with GitLab.</p>


    <h2>Getting the code</h2>

    <p>Now, go back to the main URL for your fork and clone it to your computer: open the Clone menu, and under "Open in your IDE", click "Visual Studio Code (SSH)". VSCode will open and ask you where to save the file. When VSCode asks you if you want to open the cloned repository, click the "Open" button (<b>not</b> "Add to Workspace").</p>

    <p><b>Make sure to save your project folder with a file path that does not include the <code>&amp;</code> character.</b> For example, don't save your project as <code>C:\Users\Me\Documents\Code Reading <b>&amp;</b> Review\Calculator</code>. One of the tools in our build system can't handle paths like this.</p>

    <p>Like in the TypeScript intro project, you will need to run the <code>npm i</code> command <b>once</b> to set up the project.</p>


    <h2>Building the code</h2>

    <p>For each npm console command mentioned in the README, there is an equivalent command in the VSCode "Run Build Task" menu.

    <p>Build the project and open the <code>build/index.html</code> file in a web browser to see the calculator application. Try it out! Notice the behavior if you press the = key multiple times after entering an operation, and how it updates the screen with intermediate results during complex operations with multiple operators.</p>


    <h2>Linting the code</h2>

    <p>This codebase has a code style guide which is enforced by ESLint. <b>The code that you submit must not have any ESLint warnings.</b> You must also not use any <code>eslint-disable</code> comments to disable ESLint warnings.</p>

    <p>To run ESLint from the terminal, you can run <code>npm run lint</code>. This will identify all formatting errors in your code. If you have no formatting errors, <code>npm run lint</code> will just exit without printing any warnings.</p>

    <p>To run ESLint from VSCode, you can install the <a href="https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint">ESLint plugin</a>. This will show ESLint warnings directly in the text of a file as you're editing it.</p>

    <p>Regardless of how you choose to run ESLint, <b>make sure to check that ESLint is working</b> before you submit your code. You can do this by deliberately formatting something incorrectly and checking that ESLint catches it properly.</p>


    <h2>Modifying the code</h2>

    <p>The middle two buttons in the top row on the calculator don't do anything yet. Your job is to implement their behavior.</p>

    <p>The button with the small number 2 on it is a "square" operation: when pressed, it should replace the number on screen with the result of squaring that number.</p>

    <p>The button with the % symbol on it is a "percent" symbol (not "modulo"): when pressed, it should replace the number on screen with the result of dividing that number by 100.</p>

    <p>For either button, if the screen is showing "Infinity", "-Infinity", or "NaN" before the button is pressed, it should continue showing one of these values after the button is pressed. (It is okay if pressing a button changes "Infinity" to "NaN", for example, but it shouldn't change "Infinity" to "0".)</p>

    <p>These are the only requirements for the behavior of your code. For anything left unspecified in these requirements, you can make your own decisions about how the buttons should behave. Make sure to document any of your decisions that might not be obvious!</p>

    <p>You will need to update the <code>src/Calculator.ts</code>, <code>src/CalculatorUI.ts</code>, and <code>src/Main.ts</code> in order to implement the behavior for these buttons. You will also need to add a couple lines of code to <code>html/index.html</code>, which can be copied and pasted and modified from the code that's already in there. You do not need to add unit tests in <code>src/Calculator.test.ts</code>, but it can be helpful for checking the correctness of your own code.</p>

    <p>Do not modify any existing code in this project. You'll have a chance to try to clean it up in a future assignment!</p>


    <h2>Submitting your work</h2>

    <p>You will submit your work by committing to your Git repository and pushing to your GitLab fork, which we will cover how to do in week 3 of lecture. There is nothing to submit on Canvas for this assignment.

    <p>Feel free to push multiple times, and I'll grade the current state of the repository at the time of the assignment deadline.</p>


    <h2>Grading</h2>

    <p>Your code must compile with no warnings or errors. This includes errors from ESLint: you <b>will</b> lose points for formatting or linting errors. We will cover how to use ESLint correctly in lecture.</p>

    <p>If your code compiles correctly, works correctly, and is submitted correctly, you will get an A. This assignment is just meant to get you a little bit of hands-on experience with what it looks like to extend someone's code in a modern development environment.</p>

  </body>
</html>
