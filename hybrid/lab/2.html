<h2>Introduction</h2>

<p>For our second group lab exercise, you will work with one or two other students to discuss your code from quiz 3, with the goal of building a shared understanding of the code so that you can effectively extend it as a team.</p>

<p>Unlike in lab 1, this time you will <i>not</i> be merging your code together with your teammates' code, and we will not be working with Git. Instead, you will practice "pair programming" on a new task together (which can easily be done with three people, despite the name).</p>


<h2>Reviewing each other's code</h2>

<p>First, take some time to discuss each team member's quiz 3 code. Focus on these details:</p>

<ul>
  <li>Which code did you abstract into new function definitions?</li>
  <li>What did you name your new functions?</li>
  <li>How easy is it to understand the control flow in your program?</li>
  <li>How much code reuse have you achieved? Are there still any significant repeated pieces of code left in the program?</li>
</ul>


<h2>Pair programming: preparation</h2>

<p>Next, choose one member of your group who will type (or "drive") during your pair programming work. This should ideally be the <i>least</i> experienced programmer in the group.</p>

<p>Try to avoid having a more experienced group member step in and type, <i>especially</i> when it gets frustrating: communication is part of the exercise.</p>

<p>Choose one group member's quiz 3 code that you will build on as a basis for your pair programming work, and make a copy of it. This will be your group's version of the code.</p>


<h2>Pair programming: extension</h2>

<p>Your programming task is to extend your group's codebase with two new features:</p>

<ul>
  <li>Add new functionality into the game to keep track of how many moves a player has taken.</li>
  <li>Allow the user to choose their language from a set of provided translations.</li>
</ul>

<p>Ideally, your work should also make it easier to extend the code with more new languages in the future.</p>


<h3>Move counting</h3>

<p>After the user has selected their language and entered their name, every further command that they enter should be counted as a "move". (An invalid command is still counted as a move.)</p>

<p>Each time the user makes a move, <i>before</i> outputting any other text, your code should output the total number of moves so far with <code>console.log</code>. For this part, we'll use an international symbol: the 👣 emoji will indicate the number of moves.
<blockquote>
  👣 1 <br>
  👣 2 <br>
  👣 3 <br>
  etc.
</blockquote>

<p>You can copy and paste the 👣 emoji from this page into a string in VSCode in order to print it from your program.</p>

<p>If the program is restarted by calling <code>play()</code> again, the move count should reset to zero. This means you should probably avoid global variables!</p>


<h3>Translation</h3>

<p>You can find a couple translations for each of the strings in the program at <a href="https://docs.google.com/spreadsheets/d/1w0J4vwGRBFHC2_0oY-JPvwsYinARa2ScNR4Rl_0HbEg/edit?usp=sharing">this link</a>. You're welcome to add your own translations to your code if you want, but don't let it distract you from the lab assignment!</p>

<p>Think carefully about how you might represent this spreadsheet-like structure in your code, in order to minimize code duplication.</p>

<p>Your group's code is only required to implement <i>one</i> translation other than the original English text, but you should try to design your code so that it's easy to add more translations.</p>

<p>At the very start of the game, before anything else is printed, your code should print a message like this using <code>prompt</code>:</p>
<blockquote>
  Enter EN to play in English.<br>
  Masukkan ID untuk bermain dalam Bahasa Indonesia.<br>
  「JA」を入れて日本語で遊ぶ。
</blockquote>

<p>This should only include the languages that your code actually supports. The two-letter language identifiers like EN, ID, and JA are <a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639-1</a> codes. The user will enter one of these identifiers (in uppercase) to select their language. If the user enters an invalid identifier, the program should print the string "Invalid input." in <i>every</i> language that your code supports, using a single call to <code>console.err</code> (not a separate call for each language).</p>

<p>After this point, the program should do all of its input and output in the language that the user has selected. If the program is restarted by calling <code>play()</code> again, it should ask for the user's language again.</p>


<h2>Requirements</h2>

<p>You may use any TypeScript features and any programming techniques, <b>with these limitations</b>:</p>

<ul>
  <li>Each member of your group must be able to clearly explain the features and techniques that your group uses.</li>
  <li>You must only modify <code>src/Main.ts</code>, and you may not add any new files.</li>
</ul>


<h2>Hints</h2>

Here are some hints to help get you started on adding the features described above.

<h3>Encapsulation</h3>

<p>Most parts of your code probably don't need to "know about" the move counting feature, since it's only relevant when the user is actively making a move. You might be able to find a way to structure your game code with multiple classes so that the move count variable is only visible in the parts of the code where it's relevant.</p>

<h3>Enum types</h3>

<p>TypeScript has an <code>enum</code> keyword that you may want to use in your translation code, but it's a little annoying to use. You'll probably get the best results if you define your enum like this:</p>
<pre>
enum Example {
  Foo = "Foo",
  Bar = "Bar",
  // etc.
}
</pre>

<p>With this definition of <code>Example</code>, if you have a <code>string</code> value <code>x</code>, you can write <code>if (x in Example) ...</code> to check whether <code>x</code> is one of the values defined in the enum.</p>

<h3>Interface types</h3>

<p>You might also find the <code>interface</code> keyword that we discussed in lecture to be useful. Consider that translation is a task where there is a separation between low-level concerns (choosing the string data representation of a message) and high-level concerns (communicating some kind of meaning to the user).</p>

<h3>Resource usage</h3>

<p>There's no need to hand-optimize your code in this setting: any modern browser will run this small program quickly enough for a user to be satisfied with. That said, as a matter of cleanliness, you should usually try to avoid allocating objects that will <i>never</i> be used even once. Keep this in mind if you use OOP techniques to organize your translation code.</p>


<h2>Submitting your code</h2>

<p><b>Before you leave the lab session</b>, you should have <b><i>each</i></b> group member upload a copy of the group's finished <code>Main.ts</code> file to the Canvas submission page for this lab. This is how we'll track attendance for this lab.</p>

<p>Once each member in your group has submitted to Canvas, you're done for today!</p>

<p><b>Within a week of the lab session</b>, submit your post-lab writeup to the Canvas Assignments page. The post-lab writeup is individual work that each student must submit. Review the syllabus for general details.</p>

<p>For this lab, your post-lab writeup should specifically cover:

<ul>
  <li>a reflection on how your group members' pre-lab assignment code was different from your own,</li>
  <li>a description of your group's discussions in your own words,</li>
  <li>and a description of the techniques your group used to maintain code reuse and readability while adding new features.</li>
</ul>
